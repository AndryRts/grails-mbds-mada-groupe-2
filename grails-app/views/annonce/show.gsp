<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>


    <hr>
    <div class="card-block table-border-style" style="padding-left: 2%;padding-right: 2%">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Title</label>

            <div class="col-sm-4">
                <input type="text" class="form-control form-control-round" name="title" value="${annonce.title}"
                       required="" id="title" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Description</label>

            <div class="col-sm-4">
                <input type="text" class="form-control form-control-round" name="description"
                       value="${annonce.description}"
                       required="" id="description" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Price</label>

            <div class="col-sm-4">
                <input type="number decimal" class="form-control form-control-round" name="price"
                       value="${annonce.price}"
                       required="" min="0.0" id="price" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Illustrations</label>

            <div class="col-sm-4">
                <g:each in="${annonce.illustrations}" var="illustration">
                    <img src="${"http://localhost:8081/assets/" + illustration.filename}"/>
                </g:each>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Author</label>

            <div class="col-sm-4">
                <input type="number decimal" class="form-control form-control-round" name="price"
                       value="${annonce.author.username}"
                       required="" min="0.0" id="price" disabled>
            </div>
        </div>
    </div>
<hr>




    </body>
</html>
